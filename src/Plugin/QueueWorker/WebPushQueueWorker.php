<?php

namespace Drupal\web_push\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\web_push\Service\WebPushSender;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Send push notification to subscription.
 *
 * @QueueWorker(
 *   id = "web_push",
 *   title = @Translation("Send Push Notifications"),
 * )
 */
class WebPushQueueWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The WebPushSender service.
   *
   * @var \Drupal\web_push\Service\WebPushSender
   */
  protected $sender;

  /**
   * Construct the plugin WebPushQueueWorker.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\web_push\Service\WebPushSender $sender
   *   The WebPushSender service.
   */
  public function __construct(array $configuration, string $plugin_id, mixed $plugin_definition, WebPushSender $sender) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->sender = $sender;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('web_push.sender')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($pushData) {
    $this->sender->sendNotification($pushData);
  }
}