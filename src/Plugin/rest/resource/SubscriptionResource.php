<?php

namespace Drupal\web_push\Plugin\rest\resource;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Flood\FloodInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\web_push\Form\SecurityForm;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a resource for Subscription.
 *
 * @RestResource(
 *   id = "web_push_subscription",
 *   label = @Translation("REST for Web Push subscription"),
 *   uri_paths = {
 *     "create" = "/rest/api/post/push_notifications_subscribe"
 *   }
 * )
 */
class SubscriptionResource extends ResourceBase {

  /**
   * Entity Storage to manage entity web_push_subscription.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $entityStorage;

  /**
   * The flood service.
   *
   * @var \Drupal\Core\Flood\FloodInterface
   */
  protected $flood;

  /**
   * The web_push security config object.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $securityConfig;

  /**
   * Constructs a Drupal\rest\Plugin\ResourceBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Drupal\Core\Flood\FloodInterface $flood
   *   The flood service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    EntityTypeManager $entity_type_manager,
    FloodInterface $flood,
    ConfigFactoryInterface $config_factory
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->entityStorage = $entity_type_manager->getStorage(('web_push_subscription'));
    $this->flood = $flood;
    $this->securityConfig = $config_factory->get(SecurityForm::$configId);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('web_push.rest'),
      $container->get('entity_type.manager'),
      $container->get('flood'),
      $container->get('config.factory'),
    );
  }

  /**
   * Responds to POST requests.
   *
   * Creates a new subscription entity.
   *
   * @param mixed $data
   *   Data to create the subscription.
   *
   * @return \Drupal\rest\ResourceResponse
   *   The success response.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\BadRequestHttpException
   *   Throws exception expected.
   */
  public function post(mixed $data): ResourceResponse {
    // Check flood control is enable.
    if ($this->securityConfig->get('enable')) {
      $this->flood->register('web_push.rest.post', $this->securityConfig->get('window'));

      if (!$this->flood->isAllowed('web_push.rest.post', $this->securityConfig->get('threshold'), $this->securityConfig->get('window'))) {
        // For security, just say OK.
        // Do not say "you are blocked".
        return new ResourceResponse('OK', 200);
      }
    }

    $key = $data['key'];
    $token = $data['token'];
    $endpoint = $data['endpoint'];

    if (!empty($key) && !empty($token) && !empty($endpoint)) {
      $ids = $this->entityStorage->loadByProperties([
        'key' => $key,
        'token' => $token
      ]);
      if (empty($ids)) {
        $subscription = $this->entityStorage->create([
          'key' => $key,
          'token' => $token,
          'endpoint' => $endpoint,
        ]);
        $subscription->save();
      }
    }
    else {
      throw new BadRequestHttpException();
    }

    return new ResourceResponse('OK', 200);

  }
}
