<?php

namespace Drupal\web_push\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Url;
use Drupal\Core\Queue\QueueWorkerManagerInterface;
use Drupal\web_push\Form\SettingsForm;
use Psr\Log\LoggerInterface;

/**
 * WebPush manager class.
 */
class WebPushManager {

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The web_push config object.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The queue worker manager.
   *
   * @var \Drupal\Core\Queue\QueueWorkerManagerInterface
   */
  protected $queueWorkerManager;

  /**
   * WebPushSender constructor.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Queue\QueueWorkerManagerInterface $queue_worker_manager
   *   The queue worker manager.
   */
  public function __construct(
    LoggerInterface $logger,
    ConfigFactoryInterface $config_factory,
    QueueWorkerManagerInterface $queue_worker_manager
  ) {
    $this->logger = $logger;
    $this->config = $config_factory->get(SettingsForm::$configId);
    $this->queueWorkerManager = $queue_worker_manager;
  }

  /**
   * {@inheritdoc}
   */
  /**
   * Send the notification to the queue.
   *
   * @param string $title
   *   The title of the notification.
   * @param string $body
   *   The body of the notification.
   * @param string $url
   *   The url of the notification.
   * @param string $icon
   *   The icon url of the notification.
   * @param string $urgency
   *   The urgency of the notification.
   */
  public function sendNotification(
    string $title,
    string $body,
    string $url = '',
    string $icon = '',
    string $urgency = ''
  ): void {
    if (empty($url)) {
      $url = Url::fromRoute('<front>', [], ['absolute' => TRUE])->toString();
    }
    if (empty($icon)) {
      // TODO get default icon.
    }
    $pushData = [
      'content' => [
        'title' => $title,
        'body' => $body,
        'icon' => $icon,
        'url' => $url
      ],
      'options' => [
        'urgency' => $urgency
      ],
    ];

    $worker = $this->queueWorkerManager->createInstance('web_push');
    $worker->processItem($pushData);
  }

}
