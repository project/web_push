<?php

namespace Drupal\web_push\Service;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\web_push\Entity\Subscription;
use Drupal\web_push\Form\SettingsForm;
use Drupal\web_push\Service\AuthenticationHelper;
use Minishlink\WebPush\MessageSentReport;
use Minishlink\WebPush\WebPush;
use Minishlink\WebPush\Subscription as WebPushSubscription;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * WebPush sender class.
 */
class WebPushSender {

  const URGENCY_VERY_LOW = 'very_low';
  const URGENCY_LOW = 'low';
  const URGENCY_NORMAL = 'normal';
  const URGENCY_HIGH = 'high';

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The web_push config object.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * Helper to get VAPID Keys.
   *
   * @var \Drupal\web_push\Service\AuthenticationHelper
   */
  protected $authenticationHelper;

  /**
   * Entity Storage to manage entity web_push_subscription.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $entityStorage;

  /**
   * WebPush instance.
   *
   * @var \Minishlink\WebPush\WebPush
   */
  protected $webPush;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * WebPushSender constructor.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\web_push\Service\AuthenticationHelper $authentication_helper
   *   The authentication helper for WebPush.
   * @param \Drupal\Core\Entity\EntityTypeManager $entity_type_manager
   *   The entity type manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(
    LoggerInterface $logger,
    ConfigFactoryInterface $config_factory,
    AuthenticationHelper $authentication_helper,
    EntityTypeManager $entity_type_manager,
    RequestStack $request_stack
  ) {
    $this->logger = $logger;
    $this->config = $config_factory->get(SettingsForm::$configId);
    $this->authenticationHelper = $authentication_helper;
    $this->entityStorage = $entity_type_manager->getStorage(('web_push_subscription'));
    $this->requestStack = $request_stack;
  }

  /**
   * Init the web push singleton.
   */
  protected function singletonWebPush(): void {
    if ($this->webPush) {
      return;
    }
    $auth = $this->authenticationHelper->getAuth();
    $options = [
      'TTL' => $this->config->get('ttl'),
      'urgency' => $this->config->get('urgency'),
      'topic' => $this->config->get('topic'),
      'batchSize' => (int) $this->config->get('batchSize')
    ];
    try {
      $this->webPush = new WebPush($auth, $options);
      $this->webPush->setDefaultOptions($options);
      // @see https://github.com/web-push-libs/web-push-php#reusing-vapid-headers.
      $this->webPush->setReuseVAPIDHeaders(true);
    }
    catch (\ErrorException $e) {
      $this->logger->error($e->getMessage());
    }
  }

  /**
   * Build the Subscription Notification to send.
   *
   * @param Subscription $subscription
   *   The Drupal subscription details.
   * @param array $pushData
   *   The data to send.
   *
   * @return array
   *   The Subscription notifaction array.
   */
  protected function buildSubscription(Subscription $subscription, array $pushData): array {
    $webSubscription['subscription'] = WebPushSubscription::create([
      'endpoint' => $subscription->getEndpoint(),
      'publicKey' => $subscription->getPublicKey(),
      'authToken' => $subscription->getToken()
    ]);
    // Check the URL to avoid redirection outside the site.
    $urlHost = parse_url($pushData['url'], PHP_URL_HOST);
    if (
      $urlHost
      &&
      $urlHost !== $this->requestStack->getCurrentRequest()->getHost()
    ) {
      $pushData['url'] = '';
    }
    $webSubscription['payload'] = Json::encode($pushData);
    return $webSubscription;
  }

  /**
   * Prepare the list of subscription to send.
   *
   * @param array $pushData
   *   The data content.
   * @param string $urgency
   *   The notification urgency.
   */
  protected function prepareSubscriptions(array $pushData, string $urgency = ''): void {
    $listSubscriptions = $this->entityStorage->loadMultiple();
    foreach ($listSubscriptions as $subscription) {
      $webSubscription = $this->buildSubscription($subscription, $pushData);
      $options = $this->webPush->getDefaultOptions();
      $options['urgency'] = $urgency ? $urgency : $options['urgency'];
      try {
        $this->webPush->queueNotification(
          $webSubscription['subscription'],
          $webSubscription['payload'],
          $options
        );
      }
      catch (\ErrorException $e) {
        $this->logger->error($e->getMessage());
      }
    }
  }

  /**
   * Manage action on result event.
   *
   * @param MessageSentReport $report
   *   The result of the notification send.
   */
  protected function manageReport(MessageSentReport $report): void {
    $endpoint = $report->getEndpoint();
    if ($this->config->get('debugMode')) {
      if ($report->isSuccess()) {
        $this->logger->notice("[v] Message sent successfully for subscription {$endpoint}.");
        return;
      }
      $this->logger->notice("[x] Message failed to sent for subscription {$endpoint}: {$report->getReason()}");
    }
    if ($report->isSubscriptionExpired()) {
      // Get the subscription to delete.
      // TODO : optimize to avoid the load of one entity by one.
      $subscriptionList = $this->entityStorage->loadByProperties(['endpoint' => $endpoint]);
      foreach($subscriptionList as $subscription) {
        $subscription->delete();
      }
    }
  }

  /**
   * Send the notification to all subscriber.
   *
   * @param array $pushData
   *   The data to push with options.
   */
  public function sendNotification(array $pushData): void {
    $this->singletonWebPush();
    $this->prepareSubscriptions($pushData['content'], $pushData['options']['urgency']);

    // Send the queued items by prepareSubscriptions().
    // And check message for each notifications.
    // @see https://github.com/web-push-libs/web-push-php#server-errors.
    /** @var \Minishlink\WebPush\MessageSentReport $report */
    foreach ($this->webPush->flush() as $report) {
      $this->manageReport($report);
    }
  }

}
