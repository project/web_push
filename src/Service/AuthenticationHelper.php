<?php

namespace Drupal\web_push\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Url;
use Drupal\web_push\Form\VAPIDForm;
use Minishlink\WebPush\VAPID;
use Psr\Log\LoggerInterface;

class AuthenticationHelper {

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The web_push VAPID config object.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * HelperService constructor.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(LoggerInterface $logger, ConfigFactoryInterface $config_factory) {
    $this->logger = $logger;
    $this->config = $config_factory->get(VAPIDForm::$configId);
  }


  /**
   * Generate Keys.
   *
   * @return array
   */
  public function genrateKeys() {
    try {
      return VAPID::createVapidKeys();
    }
    catch (\ErrorException $e) {
      $this->logger->error($e->getMessage());
      return [];
    }
  }

  /**
   * Build the authentitcation for push.
   *
   * @return array
   */
  public function getAuth() {
    return [
      'VAPID' => [
        'subject' => Url::fromRoute('<front>', [], ['absolute' => TRUE])->toString(),
        'publicKey' => $this->config->get('publicKey'),
        'privateKey' => $this->config->get('privateKey'),
      ]
    ];
  }
}
