<?php

namespace Drupal\web_push\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Notification subscription entity.
 *
 * @ingroup web_push
 *
 * @ContentEntityType(
 *   id = "web_push_subscription",
 *   label = @Translation("Web Push subscription"),
 *   handlers = {
 *     "storage_schema" = "Drupal\web_push\Entity\Sql\SubscriptionStorageSchema",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\web_push\Entity\SubscriptionListBuilder",
 *     "views_data" = "Drupal\web_push\Entity\SubscriptionViewsData",
 *     "form" = {
 *       "delete" = "Drupal\web_push\Form\SubscriptionDeleteForm"
 *     },
 *   },
 *   base_table = "web_push_subscription",
 *   data_table = "web_push_subscription_field_data",
 *   admin_permission = "administer web push subscriptions",
 *   entity_keys = {
 *     "id" = "id",
 *   },
 *   links = {
 *     "collection" = "/admin/content/web_push_subscription/list",
 *     "delete-form" = "/admin/content/web_push_subscription/{web_push_subscription}/delete",
 *   },
 * )
 */
class Subscription extends ContentEntityBase {

  /**
   * {@inheritdoc}
   */
  public function getPublicKey() {
    return $this->get('key')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPublicKey($key) {
    $this->set('key', $key);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getToken() {
    return $this->get('token')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setToken($token) {
    $this->set('token', $token);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getEndpoint() {
    return $this->get('endpoint')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setEndpoint($endpoint) {
    $this->set('endpoint', $endpoint);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['key'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Key'))
      ->setDescription(t('Key'))
      ->setSettings([
        'max_length' => 191,
      ])
      ->setRequired(TRUE);

    $fields['token'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Token'))
      ->setDescription(t('Token'))
      ->setSettings([
        'max_length' => 191,
      ])
      ->setRequired(TRUE);

    $fields['endpoint'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Endpoint'))
      ->setDescription(t('Communication endpoint.'))
      ->setSettings([
        'max_length' => 512,
      ])
      ->setRequired(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the subscription was created.'));

    return $fields;
  }


}
