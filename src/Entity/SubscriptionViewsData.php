<?php

namespace Drupal\web_push\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Notification subscription entities.
 */
class SubscriptionViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();
    return $data;
  }

}
