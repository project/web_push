<?php

namespace Drupal\web_push\Controller;

use Drupal\Core\Cache\CacheableResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Url;
use Drupal\web_push\Form\SettingsForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * WebPush Controller.
 *
 * Render the service worker file.
 */
class WebPushController extends ControllerBase {

  /**
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * WebPushNotificationController constructor.
   *
   * @param \Drupal\Core\Extension\ModuleHandler $moduleHandler
   *   The module handler service.
   */
  public function __construct(ModuleHandler $moduleHandler) {
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler')
    );
  }


  /**
   * Get the service worker javascript handler.
   *
   * @return \Drupal\Core\Cache\CacheableResponse
   *  The service worker content.
   */
  public function serviceWorker() {
    // Get the actual module path.
    $module_path = $this->moduleHandler->getModule('web_push')->getPath();

    // Load the file content.
    $sw = "importScripts('/" . $module_path . "/js/service_worker_notification.js');";
    $response = new CacheableResponse($sw, 200, [
      'Content-Type' => 'application/javascript',
      'Service-Worker-Allowed' => '/',
    ]);
    // TODO add cache metadata
    // $response->addCacheableDependency($cacheable_metadata);

    return $response;
  }

}
