<?php

namespace Drupal\web_push\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\web_push\Service\WebPushSender;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form to configure Default, Display and Debug Settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The config ID.
   *
   * @var string
   */
  public static $configId = 'web_push.settings';

  /**
   * Logger channel to add logs tracking.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $loggerChannel;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory
  ) {
    parent::__construct($config_factory);
    $this->loggerChannel = $this->logger('web_push');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'web_push_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::$configId
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::$configId);
    $form['options'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Default Options'),
    ];

    $form['options']['ttl'] = [
      '#type' => 'number',
      '#title' => $this->t('Time To Live'),
      '#description' => $this->t(
        "Specifying the number of seconds you want your push message to live on the push
        service before it's delivered"
      ),
      '#default_value' => $config->get('ttl') ?: 2419200,
    ];
    $form['options']['urgency'] = [
      '#type' => 'select',
      '#title' => $this->t('Urgency'),
      '#description' => $this->t(
        "Urgency indicates to the push service how important a message is to the user.
        This can be used by the push service to help conserve the battery life of a user's
        device by only waking up for important messages when battery is low."
      ),
      '#options' => [
        WebPushSender::URGENCY_VERY_LOW => $this->t('very-low'),
        WebPushSender::URGENCY_LOW => $this->t('low'),
        WebPushSender::URGENCY_NORMAL => $this->t('normal'),
        WebPushSender::URGENCY_HIGH => $this->t('high'),
      ],
      '#default_value' => $config->get('urgency') ?: WebPushSender::URGENCY_NORMAL,
    ];
    $form['options']['topic'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Topic'),
      '#description' => $this->t(
        'Topics are strings that can be used to replace a pending messages with a new message
        if they have matching topic names. This is useful in scenarios where multiple messages
        are sent while a device is offline, and you really only want a user to see the latest
        message when the device is turned on.'
      ),
      '#maxlength' => 32,
      '#default_value' => $config->get('topic') ?: '',
    ];
    $form['options']['batchSize'] = [
      '#type' => 'number',
      '#title' => $this->t('Batch size'),
      '#description' => $this->t(
        'If you send tens of thousands notifications at a time, you may get memory overflows
        due to how endpoints are called in Guzzle. In order to fix this, WebPush sends
        notifications in batches.'
      ),
      '#default_value' => $config->get('batchSize') ?: 1000,
    ];

    $form['displayBlock'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display request as a block in the page.'),
      '#default_value' => $config->get('displayBlock') ?: FALSE,
    ];

    $form['debugMode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Debug mode'),
      '#description' => $this->t(
        'This mode will add some logs, do not use in Production'
      ),
      '#default_value' => $config->get('debugMode') ?: FALSE,
    ];

    $form['actions'] = [
      '#type' => 'actions'
    ];
    $form['actions']['save'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save configurations'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(static::$configId);
    $config
      ->set('ttl', $form_state->getValue('ttl'))
      ->set('urgency', $form_state->getValue('urgency'))
      ->set('topic', $form_state->getValue('topic'))
      ->set('batchSize', $form_state->getValue('batchSize'))
      ->set('displayBlock', $form_state->getValue('displayBlock'))
      ->set('debugMode', $form_state->getValue('debugMode'))
      ->save();

    $this->loggerChannel->info(
      $this->t(
        'The userId : @user has modified the default configuration',
        ['@user' => $this->currentUser()->getAccount()->id()]
      )
    );
    parent::submitForm($form, $form_state);
  }

}
