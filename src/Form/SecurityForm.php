<?php

namespace Drupal\web_push\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form to configure Security control on REST Post.
 */
class SecurityForm extends ConfigFormBase {
  /**
   * The config ID.
   *
   * @var string
   */
  public static $configId = 'web_push.security';

  /**
   * Logger channel to add logs tracking.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $loggerChannel;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory
  ) {
    parent::__construct($config_factory);
    $this->loggerChannel = $this->logger('web_push');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'web_push_security';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::$configId
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::$configId);
    $form['flood'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Flood Control Configuration on REST'),
    ];
    $form['flood']['enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable flood control'),
      '#default_value' => $config->get('enable') ?: FALSE,
    ];
    $form['flood']['threshold'] = [
      '#type' => 'number',
      '#title' => $this->t('Threshold'),
      '#description' => $this->t(
        'The maximum number of times each user can do this event per time window'
      ),
      '#default_value' => $config->get('threshold') ?: 2,
      '#required' => TRUE,
    ];
    $form['flood']['window'] = [
      '#type' => 'number',
      '#title' => $this->t('Window'),
      '#description' => $this->t(
        'Number of seconds in the time window for this event'
      ),
      '#default_value' => $config->get('window') ?: 3600,
      '#required' => TRUE,
    ];

    $form['actions'] = [
      '#type' => 'actions'
    ];
    $form['actions']['save'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save configurations'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config(static::$configId);
    $config
      ->set('threshold', $form_state->getValue('threshold'))
      ->set('window', $form_state->getValue('window'))
      ->set('enable', $form_state->getValue('enable'))
      ->save();

    $this->loggerChannel->info(
      $this->t(
        'The userId : @user has modified the security configuration',
        ['@user' => $this->currentUser()->getAccount()->id()]
      )
    );
    parent::submitForm($form, $form_state);
  }

}
