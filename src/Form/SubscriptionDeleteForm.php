<?php

/**
 * @file
 * Contains \Drupal\web_push\Form\SubscriptionDeleteForm.
 */

namespace Drupal\web_push\Form;

use Drupal\Core\Entity\ContentEntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Provides a form for deleting a content_entity_example entity.
 *
 * @ingroup web_push
 */
class SubscriptionDeleteForm extends ContentEntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete entity %token?', array('%token' => $this->entity->getToken()));
  }

  /**
   * {@inheritdoc}
   *
   * If the delete command is canceled, return to the contact list.
   */
  public function getCancelUrl() {
    return new Url('entity.web_push_subscription.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   *
   * Delete the entity and log the event.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $entity = $this->getEntity();
    $entity->delete();

    $this->logger('web_push')->notice('deleted %token.',
      [
        '%token' => $this->entity->getToken(),
      ]
    );
    // Redirect to term list after delete.
    $form_state->setRedirect('entity.web_push_subscription.collection');
  }
}