<?php

namespace Drupal\web_push\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\web_push\Service\AuthenticationHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provide a form to configure the authentication.
 */
class VAPIDForm extends ConfigFormBase {

  /**
   * The config ID.
   *
   * @var string
   */
  public static $configId = 'web_push.vapid';

  /**
   * Logger channel to add logs tracking.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $loggerChannel;

  /**
   * Helper to generate VAPID Keys.
   *
   * @var \Drupal\web_push\Service\AuthenticationHelper
   */
  protected $authenticationHelper;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    AuthenticationHelper $authentication_helper
  ) {
    parent::__construct($config_factory);
    $this->authenticationHelper = $authentication_helper;
    $this->loggerChannel = $this->logger('web_push');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('web_push.authentication_helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'web_push_vapid';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::$configId
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::$configId);

    $form['VAPID'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Authentification (VAPID)'),
    ];
    $form['VAPID']['description'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t(
        'Do not use if you want to use automatic generate Authentication.
        More details here <a href="https://github.com/web-push-libs/web-push-php#authentication-vapid">https://github.com/web-push-libs/web-push-php#authentication-vapid</a>
        '
      ),
    ];
    $form['VAPID']['publicKey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Public Key'),
      '#description' => $this->t('(recommended) uncompressed public key P-256 encoded in Base64-URL'),
      '#default_value' => $config->get('publicKey')
    ];
    $form['VAPID']['privateKey'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Private Key'),
      '#description' => $this->t('(recommended) in fact the secret multiplier of the private key encoded in Base64-URL'),
      '#default_value' => $config->get('privateKey')
    ];

    // No method to add PEM file, PEM file is not recommended.
    // @see https://github.com/web-push-libs/web-push-php#authentication-vapid.

    $form['actions'] = [
      '#type' => 'actions'
    ];
    $form['actions']['generate'] = [
      '#type' => 'submit',
      '#value' => $this->t('Generate and Save VAPID Keys'),
      '#submit' => ['::generateVAPIDKeys']
    ];
    $form['actions']['save'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save VAPID keys'),
    ];

    return $form;
  }

  /**
   * Save the keys in config.
   *
   * @param $publicKey
   *   The public key to save.
   * @param $privateKey
   *   The private key to save.
   */
  private function saveKeys($publicKey, $privateKey): void {
    $config = $this->config(static::$configId);
    $config
      ->set('publicKey', $publicKey)
      ->set('privateKey', $privateKey)
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->saveKeys(
      $form_state->getValue('publicKey'),
      $form_state->getValue('privateKey'),
    );
    $this->loggerChannel->info(
      $this->t(
        'The userId : @user has modified the Authentication keys using manual method',
        ['@user' => $this->currentUser()->getAccount()->id()]
      )
    );
    parent::submitForm($form, $form_state);
  }

  /**
   * Genarate and save the new keys.
   */
  public function generateVAPIDKeys(): void {
    $keys = $this->authenticationHelper->genrateKeys();

    // Check we got new valid keys.
    if (empty($keys)) {
      $this->messenger()->addStatus($this->t('An error occured, the configuration was not modified. Please check the logs for details.'));
      return;
    }

    // Save the new keys.
    $this->saveKeys(
      $keys['publicKey'],
      $keys['privateKey'],
    );
    $this->messenger()->addStatus($this->t('Public and private keys have been generated.'));
    $this->loggerChannel->info(
      $this->t(
        'The userId : @user has modified the Authentication keys using Auto generate method',
        ['@user' => $this->currentUser()->getAccount()->id()]
      )
    );
  }

}
