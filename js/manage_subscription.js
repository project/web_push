/**
 * @file
 * Object to manage the subscription process.
 */
WebPushSubscription = {
  settings: [],
  urlBase64ToUint8Array : (base64String) => {
    var padding = '='.repeat((4 - base64String.length % 4) % 4);
    var base64 = (base64String + padding)
        .replace(/\-/g, '+')
        .replace(/_/g, '/');

    var rawData = window.atob(base64);
    var outputArray = new Uint8Array(rawData.length);

    for (var i = 0; i < rawData.length; ++i) {
      outputArray[i] = rawData.charCodeAt(i);
    }
    return outputArray;
  },

  /**
   * Check if we can use Push Notification
   *
   * @returns bool
   */
  checkIsAvailable: () => {
    return ('serviceWorker' in navigator) && ('PushManager' in window);
  },

  /**
   * Unsubscribe user to notification.
   *
   * @returns void
   */
  unSubscribe : () => {
    if (!WebPushSubscription.checkIsAvailable) {
      return;
    }
    navigator.serviceWorker
      .getRegistration(WebPushSubscription.settings.webPush.serviceWorkerUrl)
      .then(registration => {
        registration.pushManager.getSubscription()
        .then((subscription) => {
          subscription
            .unsubscribe()
            .then((successful) => {
              // You've successfully unsubscribed.
              console.log('Unsubscribe success');
              const event = new Event("unsubscribe");
              document.dispatchEvent(event);
            })
            .catch((e) => {
              // Unsubscribing failed
              console.log('Unsubscribe Fail');
              console.log(e);
            });
        });

      }
    );
  },

  /**
   * Subscribe user to notification.
   *
   * @returns void
   */
  subscribe : () => {
    if (!WebPushSubscription.checkIsAvailable) {
      return;
    }
    navigator.serviceWorker
    .getRegistration(WebPushSubscription.settings.webPush.serviceWorkerUrl)
    .then(registration => {

      return registration.pushManager.getSubscription()
          .then(subscription => {

            if (subscription) {
              return subscription;
            }

            var publicKey = WebPushSubscription.settings.webPush.publicKey;
            var vapidKey = WebPushSubscription.urlBase64ToUint8Array(publicKey);

            return registration.pushManager.subscribe({
              userVisibleOnly: true,
              applicationServerKey: vapidKey
            })

          });
    })
    .then(subscription => {
      const key = subscription.getKey('p256dh');
      const token = subscription.getKey('auth');
      // Get CSRF Token and then subcribe.
      fetch(Drupal.url('session/token'))
        .then(response => response.text())
        .then(csrfToken => {
          // Subscribe the user.
          fetch(
            WebPushSubscription.settings.webPush.subscribeUrl,
            {
              method: "POST",
              body: JSON.stringify({
                key: key ? btoa(String.fromCharCode.apply(null, new Uint8Array(key))) : null,
                token: token ? btoa(String.fromCharCode.apply(null, new Uint8Array(token))) : null,
                endpoint: subscription.endpoint
              }),
              headers: {
                "Content-type": "application/json; charset=UTF-8",
                'X-CSRF-Token': csrfToken
              }
            }
          ).then(() => {
            const event = new Event("subscribe");
            document.dispatchEvent(event);
          });
        }
      );
    })
    .catch((e) => {
      alert(Drupal.t('The notification service is not available'))
    })
  },

  registerSW : () => {
    if (!WebPushSubscription.checkIsAvailable) {
      return;
    }
    navigator.serviceWorker
    .register(WebPushSubscription.settings.webPush.serviceWorkerUrl);
  },



};
