/**
 * @file
 * Register service worker and ask permission for notification on page load.
 */
((Drupal) => {

  Drupal.behaviors.webPush = {

    attach: (context, settings) => {
      WebPushSubscription.settings = settings;
      const elements = once('web_push', 'body');
      elements.forEach(() => {
        WebPushSubscription.subscribe();
      });

    }
  }

})(Drupal);
