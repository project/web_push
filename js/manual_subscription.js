/**
 * @file
 * Manage subscription and unsubscription to Push notification
 */
((Drupal, once) => {
  Drupal.behaviors.webPush = {
    attach: (context, settings) => {
      const subscribeButtonList = once('webpush', document.getElementsByClassName('web-push-button subscribe'), context);
      let alreadySubscribeStatus = false;

      WebPushSubscription.settings = settings;
      WebPushSubscription.registerSW();

      /**
       * Manage display based on subscribe status.
       */
      const manageDisplay = () => {
        let buttonText = Drupal.t('Unsubscribe');
        if (alreadySubscribeStatus) {
          buttonText = Drupal.t('Subscribe');
        }
        for (let subscribeButton of subscribeButtonList) {
          subscribeButton.innerHTML = buttonText;
        }
      };

      /**
       * Init all listener.
       */
      const initListener = () => {
        for (let subscribeButton of subscribeButtonList) {
          subscribeButton.addEventListener('click', manageSubscription);
        }
        // Event send by manage_subscription.js on subscribe success.
        document.addEventListener('subscribe', () => {
          alreadySubscribeStatus = false;
          manageDisplay();
        })
        // Event send by manage_subscription.js on unsubscribe success.
        document.addEventListener('unsubscribe', () => {
          alreadySubscribeStatus = true;
          manageDisplay();
        })
      };

      /**
       * Define what to do based on subscribe status.
       */
      const manageSubscription = () => {
        if (alreadySubscribeStatus) {
          WebPushSubscription.subscribe();
        }
        else {
          WebPushSubscription.unSubscribe();
        }
      }

      /**
       * Check the subscribe status.
       */
      const alreadySubscribe = () => {
        if (!WebPushSubscription.checkIsAvailable) {
          alreadySubscribeStatus = true;
        }
        navigator.serviceWorker
          .getRegistration(WebPushSubscription.settings.webPush.serviceWorkerUrl)
          .then(registration => {
            if (!registration) {
              alreadySubscribeStatus = true;
            }
            else {
              registration.pushManager.getSubscription()
              .then((subscription) => {
                if (subscription) {
                  alreadySubscribeStatus = false;
                }
                else {
                  alreadySubscribeStatus = true;
                }
                manageDisplay();
              });
            }
          });
      };

      alreadySubscribe();
      initListener();

    },
  };
})(Drupal, once);
