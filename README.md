# Web Push

## Description
The module manage send of notification (desktop / mobile / ...) using the [Push API](https://developer.mozilla.org/en-US/docs/Web/API/Push_API).

The module allow you to:
* Configure a web push notification service
* Manage the list of subscriber
* Send notifications
* Display a bloc to subscribe and unsubscribe (Web Push block)
* Use an other Service Worker, for example if you use the [PWA module](https://www.drupal.org/project/pwa)

The module doesn't manage:
* Automatic push send, you have to define your own rules and use the service given, [see example](#example)
* Automatic deletion of invalid subscribe
* Protection against CSRF/XSRF (https://developer.mozilla.org/en-US/docs/Web/API/Push_API#push_concepts_and_usage)

Web Push use the [Push API](https://developer.mozilla.org/en-US/docs/Web/API/Push_API) to manage subscribe / unsubscribe to Push notification.

The module use the library [minishlink/web-push](https://packagist.org/packages/minishlink/web-push) to send the notifications. This library doesn't use a Third Party.

## Requirements
* php >=8.0
* [minishlink/web-push](https://packagist.org/packages/minishlink/web-push)

## Installation
* Install and enable this module like any other Drupal 8, 9 or 10 module.

## Configuration
* Configure the Authentication (VAPID) at the page /admin/config/services/web-push/VAPID, 2 methods :
  * use auto generate
  * generate it manually, [more details](https://github.com/web-push-libs/web-push-php#authentication-vapid)
* Configure the default options /admin/config/services/web-push/default-settings
* Give the permission to Access POST on REST for Web Push subscription resource

## Security advice
Security optimization:
* Rate limit on the POST request :
  * Flood control can be enable : /admin/config/services/web-push/security
* Avoid CrossDomain (CORS), server configuraton ?
* https://www.drupal.org/project/seckit

## Example
```
 \Drupal::service('web_push.manager')->sendNotification(
    'Notification Title',
    'Notification Body',
    'noticiation-redirect-url',
    'notification-icon-url',
    \Drupal\web_push\Service\WebPushSender::URGENCY_HIGH
  );
```

## Maintainers
* Yves Fierville (yfiervil) - https://www.drupal.org/u/yfiervil
